import 'package:flutter/material.dart';
import 'package:flutter_application_1/one.dart';
import 'package:flutter_application_1/three.dart';
import 'package:flutter_application_1/two.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: one(),
    );
  }
}
