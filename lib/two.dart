import 'package:flutter/material.dart';
import 'package:flutter_application_1/three.dart';

class two extends StatefulWidget {
  const two({super.key});

  State<two> createState() => onestate();
}

class onestate extends State<two> {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 242, 237, 237),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 60,
            ),
            Container(
              child: Text(
                "Let’s find your best                                                 residence ",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 23),
              child: Container(
                width: 346,
                height: 46,
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 30),
                      child: Icon(Icons.search),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      child: Text(
                        "Search your favourite paradise",
                        style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            color: Color.fromRGBO(33, 33, 33, 1)),
                      ),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              children: [
                Container(
                  child: Text(
                    "Most popular",
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
                  ),
                ),
                Spacer(),
                Container(
                  child: Text(
                    "See All",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(32, 169, 247, 1)),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    InkWell(
                      child: Container(
                        height: 306,
                        width: 211,
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 9, top: 8),
                              child: Image.asset("assets/two.png"),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 10, right: 65),
                              child: Text(
                                "Night Hill Villa",
                                style: TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.w600),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 10, right: 75),
                              child: Text(
                                "London,Night Hill",
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 10, right: 80),
                              child: Text(
                                "50000/Month",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(17)),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => three()));
                      },
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      height: 306,
                      width: 211,
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(right: 9, top: 8),
                            child: Image.asset("assets/thre.png"),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 10, right: 45),
                            child: Text(
                              "Night Hill Villa",
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 10, right: 55),
                            child: Text(
                              "London,Night Hill",
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.w500),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 10, right: 60),
                            child: Text(
                              "50000/Month",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w600),
                            ),
                          ),
                        ],
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(17)),
                    ),
                  ],
                )),
            SizedBox(
              height: 35,
            ),
            Row(
              children: [
                Container(
                  child: Text(
                    "Nearby your location",
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
                  ),
                ),
                Spacer(),
                Container(
                  child: Text(
                    "more",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(32, 169, 247, 1)),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 35,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Container(
                height: 108,
                width: 348,
                child: Row(
                  children: [
                    Container(
                      child: Image.asset("assets/four.png"),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      child: Column(
                        children: [
                          Container(
                            child: Text(
                              "Jumeriah Golf Estates Villa",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w600),
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Container(
                              child: Row(
                            children: [
                              Container(
                                child: Icon(Icons.location_city),
                              ),
                              Container(
                                child: Text(
                                  "London,Area Plam Jumeriah",
                                  style: TextStyle(
                                      fontSize: 11,
                                      fontWeight: FontWeight.w600,
                                      color: Color.fromRGBO(90, 90, 90, 1)),
                                ),
                              )
                            ],
                          )),
                          Row(
                            children: [
                              Container(
                                child: Icon(Icons.bed),
                              ),
                              Container(
                                child: Text(
                                  "4 Bedrooms",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                      color: Color.fromRGBO(90, 90, 90, 1)),
                                ),
                              ),
                              Container(
                                child: Icon(Icons.shower),
                              ),
                              Container(
                                child: Text(
                                  "5 Bathrooms",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                      color: Color.fromRGBO(90, 90, 90, 1)),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)),
              ),
            )
          ],
        ),
      ),
    );
  }
}
