import 'package:flutter/material.dart';
import 'package:flutter_application_1/two.dart';

class one extends StatefulWidget {
  const one({super.key});

  State<one> createState() => onestate();
}

class onestate extends State<one> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 232, 229, 229),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 10),
            child: Transform.scale(
              scale: 1.2, // Adjust the scale factor to your preference
              child: Image.asset(
                "assets/one.png",
              ),
            ),
          ),
          SizedBox(
            height: 70,
          ),
          Container(
            child: Text(
              "Lets find your Paradise",
              style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                  color: Colors.black),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            padding: EdgeInsets.only(left: 90),
            child: Text(
              "Find your perfect dream space                                                   with just a few clicks",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(101, 101, 101, 1)),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          InkWell(
            child: Container(
              width: 220,
              height: 55,
              decoration: BoxDecoration(
                color: Color.fromRGBO(32, 169, 247, 1),
                borderRadius: BorderRadius.circular(40),
              ),
              child: Container(
                alignment: Alignment.center,
                child: Text("Get Started",
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    )),
              ),
            ),
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => two()));
            },
          ),
        ],
      ),
    );
  }
}
